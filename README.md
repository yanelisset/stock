### Vaga de estágio BRZ: estoque

#### Objetivo:

Criar uma aplicação para gerenciar estoque de produtos com roles de acesso.

#### Credenciais de acesso:
```bash 
login: admin@mail.com 
senha: 123456
```
```bash
login: cliente@mail.com
senha: 123456
``` 

#### Requisitos: 

- [x] Criar cadastro de clientes 
- [x] Habilitar role de usuários: Administrador e cliente 
 - [x] O administrador: 
     - Controla os pedidos do estoque
     - Pode cadastrar produtos 
     - Acesso a listagem de clientes [pendente]
- [x] Pode verificar o status do pedido
- [x] Cliente: 
    - Pode realizar pedidos do estoque
    - Pode verificar o status do pedido
 - [x] Exibição de listagem de produtos 
 - [x] Criação de chat [pendente] 
 
 [Modelo Lógico](https://bitbucket.org/yanelisset/stock/src/master/modeloBD.png)

#### Setup:

##### 1. Crie a base de dados: 
```bash
$ mysql -u root -p 
> create database nomedobanco;
```
##### 2. Copie o arquivo .env.example para o .env:
```bash
$ cp .env.example .env
```
##### 3. Configure os paramêtros no arquivo .env do banco e do pusher: 
```
DB_DATABASE=nomedobanco
DB_USERNAME=root
DB_PASSWORD=suasenha

PUSHER_APP_ID= 1010719
PUSHER_APP_KEY= ac0f9b9a2bff6fe00377
PUSHER_APP_SECRET= 64c1b807fd9e2b8df170
PUSHER_APP_CLUSTER=  mt1
```
##### 4. Compile dependências do frontend: 

```bash
npm install
```
```bash
npm run watch
```
##### 5. Em outro terminal rode o script de instalação de dependências do backend:
```bash
$ sh install.sh
```

