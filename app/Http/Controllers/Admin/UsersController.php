<?php

namespace App\Http\Controllers\Admin;

use App\User;

use App\Http\Controllers\Controller;

class UsersController extends Controller
{

public function index () {
    return view('admin.users');
}

public function json () {
    if(request()->ajax()) {
        $vueTables = new EloquentVueTables;
        $data = $vueTables->get(new User, ['role']);
        return response()->json($data);
    }
    abort(401);
}
}