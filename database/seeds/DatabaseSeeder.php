<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Product::class, 20)->create();

        factory(\App\User::class, 1)->create([
            'email' => 'admin@mail.com',
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'role' => 'admin'
        ]);

        factory(\App\User::class, 1)->create([
            'email' => 'cliente@mail.com',
            'username' => 'cliente',
            'password' => bcrypt('123456'),
            'role' => 'clie'
        ]);
    }
}
