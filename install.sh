#!/bin/bash

echo "Instalando dependências do projeto (backend):"

composer install || {
    echo "Composer nao encontrado. Instalando Composer...";
    php -r "readfile('https://getcomposer.org/installer');" | php;

 } 
installback () {
	echo -e "\n Gerando migrations e seeders \n ";
	php artisan migrate ;
	php artisan migrate:fresh --seed ;
	echo -e "\n Gerando chave\n ";
	php artisan key:generate; 
	php artisan serve
}

installback

