@extends('layouts.admin')

@section('content')
    <div class="row">

      
    <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-header">
                    <h3>Formulario de produtos</h3>
                    Crie e atualize os produtos
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ $route }}" novalidate >
                        @csrf
                        @include('admin.products.form')
                    </form>
                </div>
            </div><!-- end card-->
    </div>
@endsection
