<div class="form-group">
    <label>Nome (*)</label>
    <input
        name="name"
        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
        value="{{ old('name') }}"
    />
</div>
<div class="form-group">
    <label>Descrição (*)</label>
    <textarea
        rows="3"
        class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
        name="description">{{ old('description') }}</textarea>
</div>

<div class="form-group">
    <label>Preco (*)</label>
    <input
        type="number"
        class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
        name="price"
        value="{{ old('price') }}"
    />
</div>

<button type="submit" class="btn btn-primary">{{ $btnText }}</button>
