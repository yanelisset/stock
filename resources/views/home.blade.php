@extends('layouts.admin')

@section('content')
    @auth
        <cart :user_id="{{ json_encode(auth()->id()) }}"> </cart>
    @endauth

    <products :logged="{{ json_encode(auth()->check()) }}"> </products>
@endsection