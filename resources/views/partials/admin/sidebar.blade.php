<!-- Left Sidebar -->
<div class="left main-sidebar">

    <div class="sidebar-inner leftscroll">

        <div id="sidebar-menu">

            <ul>

                <li class="submenu">
                    <a
                        href="{{ route('admin.products') }}"
                        class="{{ Helper::navigation_selected('admin.products') }}"
                    >
                        <i class="fa fa-fw fa-balance-scale"></i>
                        <span>{{ __("Produtos") }}</span>
                    </a>
                </li>

                <li class="submenu">
                    <a
                        href="{{ route('admin.orders') }}"
                        class="{{ Helper::navigation_selected('admin.orders') }}"
                    >
                        <i class="fa fa-fw fa-fire"></i>
                        <span>{{ __("Pedidos") }}</span>
                    </a>
                </li>

                <li class="submenu">
                    <a
                        href="{{ route('admin.users') }}"
                        class="{{ Helper::navigation_selected('admin.users') }}"
                    >
                    <i class="fa fa-fw fa-bars"></i>
                        <span>{{ __("Clientes") }}</span>
                    </a>
                </li>
            </ul>

        </div>

    </div>

</div>
<!-- End Sidebar -->