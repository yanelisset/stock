<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


//obter produtos para home
Route::get('/products', function () {
	return response()->json(\App\Product::all());
});

//obter produtos para users ------------------EM AJUSTE-------------------
Route::get('/users', function () {
	return response()->json(\App\User::all());
});

Route::get('admin/users', 'Admin\UsersController@index')->name('admin.users');
//

//rotas admin
Route::group(['prefix' => 'admin'], function () {
	Route::group(['prefix' => 'products'], function () {
		Route::get('/', 'Admin\ProductsController@index')->name('admin.products');
		Route::get('/data', 'Admin\ProductsController@data')->name('admin.products_data');
		Route::get('/json', 'Admin\ProductsController@json')->name('admin.products_json');
		Route::delete('/{product}', 'Admin\ProductsController@delete')->name('admin.products_delete');
		Route::get('/create', 'Admin\ProductsController@create')->name('admin.products_create');
		Route::post('/create', 'Admin\ProductsController@create')->name('admin.products_create');
		Route::post('/store', 'Admin\ProductsController@store')->name('admin.products_store');
	});

	Route::group(['prefix' => 'orders'], function () {
		Route::get('/', 'Admin\OrdersController@index')->name('admin.orders');
		Route::get('/json', 'Admin\OrdersController@json')->name('admin.orders_json');
		Route::get('/{order}', 'Admin\OrdersController@show')->name('admin.order_detail');
		Route::get('/jsonDetail/{order}', 'Admin\OrdersController@jsonDetail')->name('admin.order_json_detail');
	});

});


//rotas pedidos clientes
Route::group(['prefix' => 'orders'], function () {
	Route::get('/', 'OrdersController@index')->name('orders');
	Route::get('/json', 'OrdersController@json')->name('orders_json');
	Route::get('/{order}', 'OrdersController@show')->name('order_detail');
	Route::get('/jsonDetail/{order}', 'OrdersController@jsonDetail')->name('order_json_detail');
});

//rotas carrinho
Route::group(['prefix' => 'cart'], function () {
	Route::get('/', 'CartController@index');
	Route::post('/add-product', 'CartController@add');
	Route::post('/decrement-product', 'CartController@decrement');
	Route::delete('/remove-product/{id}', 'CartController@remove');
	Route::post('/process', 'CartController@process');
});

//rota para atualizar estado de pedido
Route::put('/order_lines/{id}/status', 'OrderLinesController@updateStatus')->name('order_lines.update_status');
